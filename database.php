<?php
interface DB{
    public function create();
    public function update();
    public function read();
    public function delete();
}
class MySQL implements DB{
    public function create()
    {
        echo 'Creating using MySQL..';
    }
    public function update()
    {
        echo 'Updating using MySQL..';
    }
    public function read()
    {
        echo 'Reading using MySQL..';
    }
    public function delete()
    {
        echo 'Deleting using MySQL..';
    }
}
class MongoDB implements DB{
    public function create()
    {
        echo 'Creating using MongoDB..';
    }
    public function update()
    {
        echo 'Updating using MongoDB..';
    }
    public function read()
    {
        echo 'Reading using MongoDB..';
    }
    public function delete()
    {
        echo 'Deleting using MongoDB..';
    }
}
class User {
    public $name;
    private $db;
    public function __construct(DB $db, $name)
    {
        $this->name = $name;
        $this->db = $db;
    }
    public function register()
    { //register a user
        $this->db->create();
    }
    public function update()
    {//update user
        $this->db->update();
    }
    public function read()
    {//read user
        $this->db->read();
    }
    public function delete()
    {//delete user
        $this->db->delete();
    }
}
$adnan = new User(new MongoDB, 'Adnan');
$adnan->register();
$adnan->update();
$adnan->read();
$adnan->delete();